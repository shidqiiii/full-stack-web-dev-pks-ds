<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function () {
    return auth()->user();
});
// Route::get('/posts', 'PostController@index');
// Route::post('/posts', 'PostController@store');

Route::apiResource('posts', 'PostController');
Route::apiResource('roles', 'RolesController');
Route::apiResource('comments', 'CommentController');

Route::group(['prefix' => 'auth', 'namespace' => 'auth'], function () {
    Route::post('/register', 'RegisterController')->name('auth.register');
    Route::post('/regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate');
    Route::post('/verification', 'VerificationController')->name('auth.verification');
    Route::post('/update-password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('/login', 'LoginController')->name('auth.login');
});
