<?php

namespace App\Listeners;

use App\Events\OtpCodeStoredEvent;
use App\Mail\OtpCodeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailToUser implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoredEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoredEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new OtpCodeMail($event->otp_code));
    }
}
