<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpCodeStoredEvent;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::create($request->all());

        do {
            $otp = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $otp)->first();
        } while ($check);

        $validUntil = Carbon::now()->addMinutes(5);
        $otp_code = OtpCode::create([
            'otp' => $otp,
            'valid_until' => $validUntil,
            'user_id' => $user->id
        ]);

        //Memanggil event
        event(new OtpCodeStoredEvent(($otp_code)));

        return response()->json([
            'success' => true,
            'message' => 'User telah dibuat, silahkan verifikasi email',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ]);
    }
}
