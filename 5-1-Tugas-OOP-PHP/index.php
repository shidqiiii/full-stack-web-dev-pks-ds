<?php

abstract class Hewan { 
    use Fight; 
    public $nama; 
    public $jumlahKaki; 
    public $keahlian = ""; 
    public $darah = 50; 
    public function __construct($nama) { 
        $this->nama = $nama; 
    } 
 
    public function atraksi() { 
        echo $this->nama . " sedang " . $this->keahlian . "<br>"; 
    } 
} 

trait Fight { 
    public $attackPower; 
    public $defensePower; 
 
    public function serang($lawan) { 
        if ($lawan->darah <= 0) { 
            echo $lawan->nama . " kalah <br>"; 
 
        } else { 
            echo $this->nama . " menyerang " . $lawan->nama . "<br>"; 
            return $lawan->diserang($this); 
        } 
    } 
 
    public function diserang($penyerang) { 
        echo $this->nama . " diserang" . "<br>"; 
        $this->darah -= ($penyerang->attackPower/$this->defensePower); 
 
        echo "Darah " . $this->nama . " sekarang " . $this->darah . "<br>"; 
 
        if ($this->darah <= 0) {
            echo $this->nama . " kalah <br>"; 
        } 
 
    } 
} 


class Harimau extends Hewan { 
    public $jumlahKaki = 4; 
    public $keahlian = "Lari cepat"; 
    public $attackPower = 7; 
    public $defensePower = 8; 
 
    public function getInfoHewan() { 
        echo "Nama Hewan: " . $this->nama . "<br>";
        echo "Jenis Hewan: " . get_class($this) . "<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br> ";
        echo "Sisa darah: " . $this->darah . "<br> ";
        echo "Attack Power: " . $this->attackPower . "<br>" ;
        echo "Defense Power: " . $this->defensePower . "<br>"; 
    } 
} 

class Elang extends Hewan { 
    public $jumlahKaki = 2; 
    public $keahlian = "terbang tinggi"; 
    public $attackPower = 10; 
    public $defensePower = 5; 
 
    public function getInfoHewan() { 
        echo "Nama Hewan: " . $this->nama . "<br>";
        echo "Jenis Hewan: " . get_class($this) . "<br>";
        echo "Jumlah kaki: " . $this->jumlahKaki . "<br>";
        echo "Keahlian: " . $this->keahlian . "<br> ";
        echo "Sisa darah: " . $this->darah . "<br> ";
        echo "Attack Power: " . $this->attackPower . "<br>" ;
        echo "Defense Power: " . $this->defensePower . "<br>"; 
    } 
} 


$harimau = new Harimau("Harimau"); 
$harimau->getInfoHewan(); 
$harimau->atraksi();
echo "<br>";
$elang = new Elang("Elang"); 
$elang->getInfoHewan(); 
$elang->atraksi(); 
echo "<br> <p>Game</p>";
for($i = 0; $i<=4; $i++){
    $harimau->serang($elang); 
    $elang->serang($harimau);
}



?>