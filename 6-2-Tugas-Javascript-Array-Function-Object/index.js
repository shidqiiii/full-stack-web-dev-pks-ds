// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort().forEach(function (item) {
    console.log(item);
})

// Soal 2
function introduce(item) {
    return "Nama saya " + item.name + ", umur saya " + item.age + " tahun, alamat saya di " + item.address + ", dan saya punya hobby yaitu " + item.hobby;
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

// Soal 3
function hitung_huruf_vokal(str) {

    // const count = str.match(/[aeiou]/gi).length;
    // return count;

    let count = 0;
    const vowels = ["a", "e", "i", "o", "u"];

    for (let letter of str.toLowerCase()) {
        if (vowels.includes(letter)) {
            count++;
        }
    }
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1, hitung_2) // 3 2

//Soal 4
function hitung(number) {
    return (number * 2) - 2;
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8